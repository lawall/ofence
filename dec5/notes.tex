\documentclass{article}
\usepackage{fullpage}
\usepackage{listings}
\usepackage{xcolor}

\newcommand{\extrabold}{}%{\bfseries}

  \lstdefinelanguage{diff}{
	basicstyle=\ttfamily\extrabold\scriptsize,
	morecomment=[f][\color{subtitlex}]{@},
	morecomment=[f][\color{gr}]{+},
	morecomment=[f][\color{red}]{-},
	morecomment=[f][\color{purple}]{*},
	morecomment=[f][\color{purple}]{?},
        keepspaces=true,
	escapechar=£,
	identifierstyle=\color{black},
  }

  \lstdefinelanguage{tdiff}{
	basicstyle=\ttfamily\extrabold\tiny,
	morecomment=[f][\color{subtitlex}]{@},
	morecomment=[f][\color{gr}]{+},
	morecomment=[f][\color{red}]{-},
        keepspaces=true,
	escapechar=£,
	identifierstyle=\color{black},
  }

\lstloadlanguages{C}
\lstset{language=C,
	basicstyle=\ttfamily\extrabold\scriptsize,
	backgroundcolor=\color{white},
        showspaces=false,
        rulesepcolor=\color{gray},
	showstringspaces=false,
	keywordstyle=\bfseries\color{blue!40!black},
	commentstyle=\itshape\color{purple!40!black},
	identifierstyle=\color{black},
	stringstyle=\color{red},
	escapechar=£,
        numbers=none,
        numbersep=2pt,
        morekeywords={elif},
       }

\title{Some possible errors in barrier usage in Linux \newline (linux-next,
  Dec 2, 2023)}
\date{December 5, 2023}

\begin{document}

\maketitle

\section{Overview}

Report on a discussion of potential barrier misuse examples on December 5,
2023.  Participants: Julia Lawall and Paul McKenney.

\section{drivers/mmc/host/atmel-mci.c}

\subsection{Scenario}

We consider the following functions:

\begin{lstlisting}[language=C]
static void atmci_cleanup_slot(struct atmel_mci_slot *slot,
                unsigned int id)
{
        /* Debugfs stuff is cleaned up by mmc core */

	set_bit(ATMCI_SHUTDOWN, &slot->flags);
        smp_wmb();

        mmc_remove_host(slot->mmc);

	if (gpio_is_valid(slot->detect_pin)) {
                int pin = slot->detect_pin;

                free_irq(gpio_to_irq(pin), slot);
                del_timer_sync(&slot->detect_timer);
	}

        slot->host->slot[id] = NULL;
	mmc_free_host(slot->mmc);
}

static void atmci_detect_change(struct timer_list *t)
{
        struct atmel_mci_slot   *slot = from_timer(slot, t, detect_timer);
        bool                    present;
        bool                    present_old;

        /*
         * atmci_cleanup_slot() sets the ATMCI_SHUTDOWN flag before
         * freeing the interrupt. We must not re-enable the interrupt
         * if it has been freed, and if we're shutting down, it
         * doesn't really matter whether the card is present or not.
         */
        smp_rmb();
        if (test_bit(ATMCI_SHUTDOWN, &slot->flags))
                return;

        enable_irq(gpio_to_irq(slot->detect_pin));
	...
}
\end{lstlisting}

These two functions contain the only mention of {\tt ATMCI\_SHUTDOWN}.

The {\tt smp\_wmb()} was introduced in commit
965ebf33ea5afb6386f5b57cc71e6572253746b3 from 2008 that introduced the
function {\tt atmci\_cleanup\_slot}.  This commit also mentioned that it
was cleaning up the locking generally in the file, and added quite a lot of
{\tt smp\_wmb()}s elsewhere in the file.

The {\tt smp\_rmb()} was introduced previously, in commit
7d2be0749a59096a334c94dc48f43294193cb8ed, also from 2008, when the driver
was introduced.

\subsection{Analysis}

The first question was whether these functions can run concurrently.  The
function {\tt atmci\_detect\_change} is run as a handler on the expiration
of a timer, and the function {\tt atmci\_cleanup\_slot} contains a call to
{\tt del\_timer\_sync} to deactivate a timer and wait for the handler to
finish, which suggests that they can run concurrently.

Note that the timer is only set up if the condition {\tt
  gpio\_is\_valid(slot->detect\_pin)} holds, so we can only be concerned with
that case.

Next the concern is the call to {\tt free\_irq} and the call to {\tt
  enable\_irq}.  It seems like it would be undesirable for these two calls
to happen concurrently.

Finally, we can consider the possible orders of operations:

\vspace{\baselineskip}

\begin{tabular}{lcl}
atmci\_cleanup\_slot && atmci\_detect\_change \\\hline
&& {\tt smp\_rmb();} \\
&& {\tt if (test\_bit(ATMCI\_SHUTDOWN, \&slot->flags))} \\
{\tt set\_bit(ATMCI\_SHUTDOWN, \&slot->flags);} && \\
{\tt smp\_wmb();} && \\
\ldots && \\
{\tt free\_irq(gpio\_to\_irq(pin), slot);} && {\tt
  enable\_irq(gpio\_to\_irq(slot->detect\_pin));}
\end{tabular}

\vspace{\baselineskip}

The {\tt smp\_rmb()} has no effect because there have been no prior
relevant reads.  The test of the {\tt ATMCI\_SHUTDOWN} bit sees the old
value.  The {\tt smp\_wmb()} has no effect because no one will be looking
at the {\tt ATMCI\_SHUTDOWN} bit again.  Nothing prevents the {\tt
  free\_irq} and the {\tt enable\_irq} from occurring at the same time.

\section{drivers/net/ethernet/qlogic/qed/qed\_ll2.c}

\subsection{Scenario}

We consider the following functions:

\begin{lstlisting}[language=C]
int qed_ll2_terminate_connection(void *cxt, u8 connection_handle)
{
	... // code similar to the following, but for TX
        if (QED_LL2_RX_REGISTERED(p_ll2_conn)) {
                p_ll2_conn->rx_queue.b_cb_registered = false;
                smp_wmb(); /* Make sure this is seen by ll2_lb_rxq_completion */

                if (p_ll2_conn->rx_queue.ctx_based)
                        qed_db_recovery_del(p_hwfn->cdev,
                                            p_ll2_conn->rx_queue.set_prod_addr,
                                            &p_ll2_conn->rx_queue.db_data);

                rc = qed_sp_ll2_rx_queue_stop(p_hwfn, p_ll2_conn);
                if (rc)
                        goto out;

                qed_ll2_rxq_flush(p_hwfn, connection_handle);
                qed_int_unregister_cb(p_hwfn, p_ll2_conn->rx_queue.rx_sb_index);
        }
	...
}

static int qed_ll2_lb_rxq_completion(struct qed_hwfn *p_hwfn, void *p_cookie)
{
        struct qed_ll2_info *p_ll2_conn = (struct qed_ll2_info *)p_cookie;
        int rc;

	if (!p_ll2_conn)
                return 0;

        if (!QED_LL2_RX_REGISTERED(p_ll2_conn))
                return 0;

	rc = qed_ll2_lb_rxq_handler(p_hwfn, p_ll2_conn);
        if (rc)
               	return rc;

	qed_ooo_submit_rx_buffers(p_hwfn, p_ll2_conn);
        qed_ooo_submit_tx_buffers(p_hwfn, p_ll2_conn);

        return 0;
}
\end{lstlisting}

There appears to be a typo in the code, because the TX code (not shown)
has the same comment as the one shown for RX code, referring to the {\tt
  qed\_ll2\_lb\_rxq\_completion}.  But that function doesn't refer to
QED\_LL2\_TX\_REGISTERED, which is a macro accessing the {\tt
  tx\_queue.b\_cb\_registered} field.

We focus on the RX case, because the completion function is shorter.

The {\tt smp\_wmb()} was introduced in commit
490068deaef0c76e47bf89c457de899b7d3995c7 in 2018.  That commit had the
subject line ``Fix LL2 race during connection terminate''.  The changes
just introduce the assignments to false and the calls to {\tt
  qed\_int\_unregister\_cb}, and introduce the calls to {\tt smp\_wmb()}.

\subsection{Analysis}

This case is the same as the previous one.  Execution can start in the
completion function, up through the test of {\tt QED\_LL2\_RX\_REGISTERED}.
{\tt QED\_LL2\_RX\_REGISTERED(p\_ll2\_conn)} still returns true, so the
terminate connection function can now execute, setting {\tt
  p\_ll2\_conn->rx\_queue.b\_cb\_registered = false}.  The {\tt smp\_wmb()}
then has no impact, because {\tt QED\_LL2\_RX\_REGISTERED(p\_ll2\_conn)}
has already been tested by the completion function.

To solve the problem, one could consider whether locks can be used are each
of the shown blocks of code.  But the completion functions are callbacks,
which might make locks undesirable.  Another option could be to exploit
reference counting, and register the code of the terminate connection
function as something to do when the reference count reaches 0.

\section{drivers/video/fbdev/omap2/omapfb/dss/dispc.c}

\subsection{Scenario}

We consider the following functions:

\begin{lstlisting}[language=C]
int dispc_request_irq(irq_handler_t handler, void *dev_id)
{
        int r;

	if (dispc.user_handler != NULL)
                return -EBUSY;

	dispc.user_handler = handler;
        dispc.user_data = dev_id;

	/* ensure the dispc_irq_handler sees the values above */
        smp_wmb();

	r = devm_request_irq(&dispc.pdev->dev, dispc.irq, dispc_irq_handler,
                             IRQF_SHARED, "OMAP DISPC", &dispc);
	if (r) {
                dispc.user_handler = NULL;
                dispc.user_data = NULL;
	}

	return r;
}

static irqreturn_t dispc_irq_handler(int irq, void *arg)
{
 	if (!dispc.is_enabled)
                return IRQ_NONE;

	return dispc.user_handler(irq, dispc.user_data);
}
\end{lstlisting}

The {\tt smp\_wmb()} has been there since the driver was introduced.

\subsection{Analysis}

Unfolding {\tt devm\_request\_irq} reveals a {\tt mutex\_lock}, which
apparently contains a full
barrier.\footnote{https://stackoverflow.com/questions/33754443/is-linux-mutex-lock-implemented-using-memory-barrier}
Therefore, the {\tt smp\_wmb()} is not needed.  Indeed, initializing the
fields of the structure prepared for a callback function is completely
standard behavior, and it would be strange if the code were required to
explicitly add a barrier at every initialization site.

\section{drivers/net/ethernet/nxp/lpc\_eth.c}

\subsection{Scenario}

We consider the following functions:

\begin{lstlisting}[language=C]
static void __lpc_eth_init(struct netdata_local *pldat)
{
	...
	/* Clear and enable interrupts */
        writel(0xFFFF, LPC_ENET_INTCLEAR(pldat->net_base));
	smp_wmb();
        lpc_eth_enable_int(pldat->net_base);
	...
}

static void lpc_eth_enable_int(void __iomem *regbase)
{
 	writel((LPC_MACINT_RXDONEINTEN | LPC_MACINT_TXDONEINTEN),
               LPC_ENET_INTENABLE(regbase));
}
\end{lstlisting}

The entire function is full of {\tt readl} and {\tt writel}.

The {\tt smp\_wmb()} has been there since the initial commit, b7370112f5195,
in 2012.

\subsection{Analysis}

The goal seems to be to ensure that the first {\tt writel} happens before
the second one.  But {\tt smp\_wmb()} doesn't help with that. {\tt wmb()}
should be used instead.

\end{document}
