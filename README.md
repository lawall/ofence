# ofence

Repository of examples building on the following paper:

Baptiste Lepers, Josselin Giet, Willy Zwaenepoel, Julia Lawall:
OFence: Pairing Barriers to Find Concurrency Bugs in the Linux Kernel. EuroSys 2023: 33-45
